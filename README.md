INTRODUCTION
------------

This module provides a VBO action to export displayed view rows to csv file.


INSTALLATION
------------

Install as any other module.


USAGE
-----

Follow guidelines for adding a new view action, refer to the Views Bulk operations module documentation https://www.drupal.org/project/views_bulk_operations or chcek under view fields -> Bulk operations :)
