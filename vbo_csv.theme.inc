<?php

/**
 * @file
 * Contains module theme functions.
 */

/**
 * Csv content builder function.
 */
function theme_vbo_csv($variables) {
  // Sanitize data.
  foreach ($variables['header'] as $key => $item) {
    $variables['header'][$key] = strtr($item, array($variables['separator'] => ' '));
  }

  $content_replacements = array(
    '\r\n' => ' ',
    '\n\r' => ' ',
    '\r' => ' ',
    '\n' => ' ',
    '\t' => ' ',
    $variables['separator'] => ' '
  );
  foreach ($variables['rows'] as $row_key => $row) {
    foreach ($row as $cell_key => $cell) {
      $variables['rows'][$row_key][$cell_key] = strtr($cell, $content_replacements);
    }
  }

  // Generate output.
  $csv_rows = array();
  $csv_rows[] = implode(';', $variables['header']);
  foreach ($variables['rows'] as $row) {
    $csv_rows[] = implode(';', $row);
  }

  return implode(PHP_EOL, $csv_rows);
}
